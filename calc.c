#define _GNU_SOURCE
#include <byteswap.h>
#include <fcntl.h>
#include <immintrin.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
/* #include <sys/uio.h> */

struct uint8_array {
  uint8_t *buf;
  size_t len;
};

struct int32_array {
  int32_t *buf;
  size_t len;
};

void write_to_index(int32_t bits, struct int32_array *index, int base) {
  int cnt = __builtin_popcount(bits);

  for (int i = 0; i < 8; i++) {
    int n = __builtin_ctz(bits);
    bits = __blsr_u32(bits);
    index->buf[index->len + i] = base + n;
  }

  if (__builtin_expect(cnt > 8, 0)) {
    int i = 8;
    do {
      int n = __builtin_ctz(bits);
      bits = __blsr_u32(bits);
      index->buf[index->len + i] = base + n;
      i++;
    } while (i < cnt);
  }
  index->len += cnt;
}

uint8_t stage1(const struct uint8_array *buffer, uint8_t carry, int base,
               struct int32_array *num_index,
               struct int32_array *non_num_index,
               struct int32_array *newline_index) {
  off_t offset = 0;
  const __m256i newline_16times = _mm256_set1_epi8('\n');
  const __m128i newline_8times = _mm_set1_epi8('\n');
  const __m256i space = _mm256_set1_epi8(' ');
  const __m256i plus = _mm256_set1_epi8('+');
  const __m256i before_a0_16times = _mm256_set1_epi8('0' - 1);
  const __m256i after_a9_16times = _mm256_set1_epi8('9' + 1);
  const __m128i before_a0_8times = _mm_set1_epi8('0' - 1);
  const __m128i after_a9_8times = _mm_set1_epi8('9' + 1);
  int num_spaces = 0;
  int num_pluses = 0;
  offset = base;
  while (offset + 32 <= buffer->len) {
    __m256i batch = _mm256_loadu_si256((__m256i const *)(buffer->buf + offset));

    /* __m256i is_space = _mm256_cmpeq_epi8(batch, space); */
    /* int is_space_mask = _mm256_movemask_epi8(is_space); */
    /* num_spaces += __builtin_popcount(is_space_mask); */

    /* __m256i is_plus = _mm256_cmpeq_epi8(batch, plus); */
    /* int is_plus_mask = _mm256_movemask_epi8(is_plus); */
    /* num_pluses += __builtin_popcount(is_plus_mask); */

    uint32_t num_mask =
        _mm256_movemask_epi8(_mm256_cmpgt_epi8(batch, before_a0_16times)) &
        _mm256_movemask_epi8(_mm256_cmpgt_epi8(after_a9_16times, batch));
    uint32_t non_num_mask = ~num_mask;

    uint32_t num_mask_shifted = num_mask << 1 | carry;
    uint32_t num_starts_mask = (num_mask ^ num_mask_shifted) & num_mask;
    write_to_index(num_starts_mask, num_index, offset);

    uint32_t non_num_mask_shifted = non_num_mask << 1 | (!carry);
    uint32_t non_num_starts_mask = (non_num_mask ^ non_num_mask_shifted) & non_num_mask;
    write_to_index(non_num_starts_mask, non_num_index, offset);

    carry = num_mask >> 31;

    __m256i is_newline = _mm256_cmpeq_epi8(batch, newline_16times);
    uint32_t is_newline_mask = _mm256_movemask_epi8(is_newline);
    write_to_index(is_newline_mask, newline_index, offset);
    /* int invalid_mask = ~(is_space_mask | is_plus_mask | num_mask |
     * is_newline_mask); */
    /* if (invalid_mask) { */
    /*   printf("is_space_mask: %x is_plus_mask: %x invalid: %x\n",
     * is_space_mask, is_plus_mask, invalid_mask); */
    /* } */

    /* if (is_newline_mask) { */
    /*   printf("%d spaces %d pluses\n", num_spaces, num_pluses); */
    /*   num_spaces = 0; */
    /*   num_pluses = 0; */
    /* } */
    offset += 32;
  }

  if (offset + 16 <= buffer->len) {
    __m128i batch = _mm_loadu_si128((__m128i const *)(buffer->buf + offset));

    uint16_t num_mask =
        _mm_movemask_epi8(_mm_cmpgt_epi8(batch, before_a0_8times)) &
        _mm_movemask_epi8(_mm_cmpgt_epi8(after_a9_8times, batch));
    uint16_t non_num_mask = ~num_mask;
    uint16_t num_mask_shifted = num_mask << 1 | carry;
    uint16_t num_starts_mask = (num_mask ^ num_mask_shifted) & num_mask;
    write_to_index(num_starts_mask, num_index, offset);

    uint16_t non_num_mask_shifted = non_num_mask << 1 | (!carry);
    uint16_t non_num_starts_mask = (non_num_mask ^ non_num_mask_shifted) & non_num_mask;
    write_to_index(non_num_starts_mask, non_num_index, offset);

    carry = num_mask >> 15;

    __m128i is_newline = _mm_cmpeq_epi8(batch, newline_8times);
    uint32_t is_newline_mask = _mm_movemask_epi8(is_newline);
    write_to_index(is_newline_mask, newline_index, offset);

    offset += 16;
  }

  while (offset < buffer->len) {
    uint8_t is_num = buffer->buf[offset] >= '0' && buffer->buf[offset] <= '9';
    if (!carry && is_num) {
      num_index->buf[num_index->len++] = offset;
    } else {
      if (carry && !is_num) {
        non_num_index->buf[non_num_index->len++] = offset;
      }
      if (buffer->buf[offset] == '\n') {
        newline_index->buf[newline_index->len++] = offset;
      }
    }
    carry = is_num;
    offset++;
  }
  return carry;
}

int parse_digit(const uint8_t c, uint64_t *i) {
  const uint64_t digit = c - '0';
  if (digit > 9) {
    return 0;
  }
  *i = 10 * *i + digit;
  return 1;
}

uint64_t parse_num(const uint8_t *src) {
  uint64_t i = 0;
  while (parse_digit(*src, &i)) {
    src++;
  }
  return i;
}


static __m128i shuffle_masks[] = {
  {0xffffffffffffffff, 0xffffffffffffffff},
  {0x00ffffffffffffff, 0xffffffffffffffff},
  {0x0100ffffffffffff, 0xffffffffffffffff},
  {0x020100ffffffffff, 0xffffffffffffffff},
  {0x03020100ffffffff, 0xffffffffffffffff},
  {0x0403020100ffffff, 0xffffffffffffffff},
  {0x050403020100ffff, 0xffffffffffffffff},
  {0x06050403020100ff, 0xffffffffffffffff},
  {0x0706050403020100, 0xffffffffffffffff},
};

uint64_t parse_num_swar(const uint8_t *src, int len) {
  const __m128i ascii0 = _mm_set1_epi8('0');
  const __m128i mul_1_10 =
    _mm_setr_epi8(10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1);
  const __m128i mul_1_100 = _mm_setr_epi16(100, 1, 100, 1, 100, 1, 100, 1);
  const __m128i mul_1_10000 =
    _mm_setr_epi16(10000, 1, 10000, 1, 10000, 1, 10000, 1);
  if (__builtin_expect(len <= 8, 1)) {
    __m128i input = _mm_loadu_si128((const __m128i *)(src));
    input = _mm_shuffle_epi8(input, shuffle_masks[len]);
    input = _mm_subs_epu8(input, ascii0);
    const __m128i t1 = _mm_maddubs_epi16(input, mul_1_10);
    const __m128i t2 = _mm_madd_epi16(t1, mul_1_100);
    const __m128i t3 = _mm_packus_epi32(t2, t2);
    const __m128i t4 = _mm_madd_epi16(t3, mul_1_10000);
    return _mm_cvtsi128_si32(t4); // only captures the sum of the first 8 digits, drop the rest
  } else {
    return parse_num(src);
  }
}

void stage2(const struct uint8_array *char_buf,
           const struct int32_array *num_start_index,
           const struct int32_array *non_num_start_index,
           const struct int32_array *newline_index,
           uint64_t *tail_sum,
           int32_t *partial_num_index,
           int32_t *partial_non_num_index) {
  uint64_t sum = *tail_sum;
  int num_id = 0;
  int non_num_id = 0;
  while (non_num_id < non_num_start_index->len && non_num_start_index->buf[non_num_id] < num_start_index->buf[num_id]) {
    non_num_id++;
  }
  for (int line_id = 0; line_id < newline_index->len; line_id++) {
    int newline_pos = newline_index->buf[line_id];
    while (num_id < num_start_index->len &&
           num_start_index->buf[num_id] < newline_pos) {
      int ndigits = non_num_start_index->buf[non_num_id] - num_start_index->buf[num_id];
      /* uint64_t num = parse_num(char_buf->buf + num_start_index->buf[num_id]); */
      uint64_t num = parse_num_swar(char_buf->buf + num_start_index->buf[num_id], ndigits);
      /* fprintf(stdout, "%lu ", num); */
      sum += num;
      num_id++;
      non_num_id++;
    }
    fprintf(stdout, "%lu\n", sum);
    sum = 0;
  }
  *tail_sum = 0;
  if (__builtin_expect(num_id < num_start_index->len, 1)) {
    while (num_id < num_start_index->len - 1) {
      uint64_t num = parse_num(char_buf->buf + num_start_index->buf[num_id]);
      *tail_sum += num;
      num_id++;
    }
  }
  *partial_num_index = num_id;
  *partial_non_num_index = non_num_id;
}

#define BLOCK_SZ (1024 * 1024)

int main() {
  struct uint8_array char_buf;
  char_buf.buf = malloc(BLOCK_SZ + 10);
  char_buf.len = 0;

  struct int32_array number_index;
  number_index.buf = malloc(BLOCK_SZ * sizeof(int32_t));
  number_index.len = 0;

  struct int32_array non_number_index;
  non_number_index.buf = malloc(BLOCK_SZ * sizeof(int32_t));
  non_number_index.len = 0;

  struct int32_array newline_index;
  newline_index.buf = malloc(BLOCK_SZ * sizeof(int32_t));
  newline_index.len = 0;

  ssize_t n;
  uint8_t carry = 0;
  uint64_t tail_sum = 0;
  do {
    ssize_t total_read = char_buf.len;
    do {
      n = read(0, char_buf.buf + char_buf.len,
               BLOCK_SZ - total_read);
      char_buf.len += n;
      if (n == -1) {
        perror("read");
        return -1;
      }
      carry = stage1(&char_buf, carry, total_read,
                     &number_index, &non_number_index, &newline_index);
      total_read += n;
    } while (n > 0 && char_buf.len + 65536 <= BLOCK_SZ);
    int partial_num_id, partial_non_num_id;
    stage2(&char_buf, &number_index, &non_number_index,
           &newline_index, &tail_sum, &partial_num_id, &partial_non_num_id);
    if (__builtin_expect(partial_num_id < number_index.len, 1)) {
      int remainder_pos = number_index.buf[partial_num_id];
      int remainder_size = char_buf.len - remainder_pos;
      memcpy(char_buf.buf,
             &char_buf.buf[remainder_pos], remainder_size);
      char_buf.len = remainder_size;
      for (int i = 0; i < number_index.len - partial_num_id; i++) {
        number_index.buf[i] =
          number_index.buf[partial_num_id + i] - remainder_pos;
      }
      number_index.len = number_index.len - partial_num_id;

      if (partial_non_num_id < non_number_index.len) {
        for (int i = 0; i < non_number_index.len - partial_non_num_id; i++) {
          non_number_index.buf[i] =
            non_number_index.buf[partial_non_num_id + i] - remainder_pos;
        }
        non_number_index.len = non_number_index.len - partial_non_num_id;
      } else {
        non_number_index.len = 0;
      }
    } else {
      char_buf.len = 0;
      number_index.len = 0;
      non_number_index.len = 0;
    }
    newline_index.len = 0;
  } while (n > 0);
  return 0;
}
